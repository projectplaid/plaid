# Plaid

The Project Plaid kernel

## Dependencies

* [Alire](https://alire.ada.dev/) as a build tool. This will handle installing a GNAT toolchain for you.
* [qemu](https://www.qemu.org/) as a emulator.

## Targets

The initial target for this kernel is RISC-V (rv64gc), specifically the qemu-system-riscv64 "virt" machine,
until such time as relevant hardware is available.

## License

There are multiple licenses in use on this project:

* The core of the project is under [MIT license](https://opensource.org/license/mit/).
* The RTS portion is under [GNU General Public License v3](https://www.gnu.org/licenses/gpl-3.0.en.html)
  with the GCC Runtime Library Exception to match the license of its original source.
