with System.Machine_Code;

package body Arch is
   procedure Init is
   begin
      null;
   end Init;

   procedure Disable_IRQ is
      use System.Machine_Code;
   begin
      Asm
        (Template => "csrw mie, zero", Clobber => "memory", Volatile => True);
   end Disable_IRQ;

   procedure Enable_IRQ is
   begin
      null;
   end Enable_IRQ;

end Arch;
