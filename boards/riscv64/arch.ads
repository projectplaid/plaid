package Arch is
   procedure Init;

   procedure Disable_IRQ;
   procedure Enable_IRQ;
end Arch;
