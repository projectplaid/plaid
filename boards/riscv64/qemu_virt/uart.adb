with Interfaces;
with System;
with System.Storage_Elements; use System.Storage_Elements;

package body UART is
   use type Interfaces.Unsigned_8;

   Base_Address : constant System.Address := System'To_Address (16#1000_0000#);

   Data : Interfaces.Unsigned_8 with
     Address => Base_Address + 16#00#;
   LSR  : Interfaces.Unsigned_8 with
     Address => Base_Address + 16#14#;

   LSR_THRE : constant Interfaces.Unsigned_8 := 2#0010_0000#;
   LSR_DR   : constant Interfaces.Unsigned_8 := 2#0000_0001#;

   ---------
   -- Get --
   ---------

   function Get return Character is
   begin
      return Character'Val (Data);
   end Get;

   ----------------
   -- Initialize --
   ----------------

   procedure Initialize is
   begin
      Initialized := True;
   end Initialize;

   -----------------
   -- Is_Rx_Ready --
   -----------------

   function Is_Rx_Ready return Boolean is
   begin
      return (LSR and LSR_DR) /= 0;
   end Is_Rx_Ready;

   -----------------
   -- Is_Tx_Ready --
   -----------------

   function Is_Tx_Ready return Boolean is
   begin
      return (LSR and LSR_THRE) /= 0;
   end Is_Tx_Ready;

   ---------
   -- Put --
   ---------

   procedure Put_Char (Ch : Character) is
   begin
      Data := Character'Pos (Ch);
   end Put_Char;

   ----------------------------
   -- Use_Cr_Lf_For_New_Line --
   ----------------------------

   function Use_Cr_Lf_For_New_Line return Boolean is
   begin
      return True;
   end Use_Cr_Lf_For_New_Line;
end UART;
