package UART is
   procedure Initialize;
   function Get return Character;
   function Is_Rx_Ready return Boolean;
   function Is_Tx_Ready return Boolean;
   function Use_Cr_Lf_For_New_Line return Boolean;

   procedure Put_Char (Ch : Character);
   pragma Export (C, Put_Char, "putchar");

   Initialized : Boolean := False;

end UART;
