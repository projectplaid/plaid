#!/bin/bash

APP_BINARY=$(find bin -name plaid)

qemu-system-riscv64 \
    -m 2G \
	-nographic \
	-machine virt \
	-bios none \
	-kernel ${APP_BINARY} \
	$*

