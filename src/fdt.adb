package body FDT is

   function Get_Header_Size (FDT_Addr : System.Address) return Unsigned_32 is
      function Header_Size (Version : Unsigned_32) return Unsigned_32 is
      begin
         if Version <= 1 then
            return FDT_V1_Size;
         elsif Version = 2 then
            return FDT_V2_Size;
         elsif Version = 3 then
            return FDT_V3_Size;
         elsif Version <= 16 then
            return FDT_V16_Size;
         end if;

         return FDT_V17_Size;
      end Header_Size;
   begin
      if Can_Assume (Assume_Latest) then
         return FDT_V17_Size;
      else
         return Header_Size (FDT_Version (FDT_Addr));
      end if;
   end Get_Header_Size;

   function Can_Assume (Mask : Assumption_Mask) return Boolean is
   begin
      return (Assume_Mask and Mask) = Mask;
   end Can_Assume;

   function FDT_Version (FDT_Addr : System.Address) return Unsigned_32 is
   begin
      return 17;
   end FDT_Version;
end FDT;
