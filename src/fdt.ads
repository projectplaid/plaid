with System;
with Interfaces; use Interfaces;

with Ada.Unchecked_Conversion;

package FDT is
   type Header is record
      Magic                     : Unsigned_32;
      Total_Size                : Unsigned_32;
      Offset_DT_Struct          : Unsigned_32;
      Offset_DT_Strings         : Unsigned_32;
      Offset_Memory_Reserve_Map : Unsigned_32;
      Version                   : Unsigned_32;

      --  Version 2 fields
      Boot_CPUID_Physical : Unsigned_32;

      --  Version 3 fields
      Size_DT_Strings : Unsigned_32;
      Size_DT_Struct  : Unsigned_32;
   end record with
     Pack;

   type Header_Access is access Header;

   function To_Header_Access is new Ada.Unchecked_Conversion
     (System.Address, Header_Access);

   type Reserve_Entry is record
      Addr : Unsigned_64;
      Size : Unsigned_64;
   end record with
     Pack;

   type Reserve_Entry_Access is access Reserve_Entry;

   function To_Reserve_Entry is new Ada.Unchecked_Conversion
     (System.Address, Reserve_Entry_Access);

   type Node_Header (Name_Size : Integer) is record
      Tag  : Unsigned_32;
      Name : String (1 .. Name_Size);
   end record with
     Pack;

   type Node_Header_Access is access Node_Header;

   function To_Node_Header is new Ada.Unchecked_Conversion
     (System.Address, Node_Header_Access);

   type FDT_Property (Data_Size : Integer) is record
      Tag         : Unsigned_32;
      Len         : Unsigned_32;
      Name_Offset : Unsigned_32;
      Data        : String (1 .. Data_Size);
   end record with
     Pack;

   type FDT_Property_Access is access FDT_Property;

   function To_FDT_Property is new Ada.Unchecked_Conversion
     (System.Address, FDT_Property_Access);

   FDT_Magic    : constant Unsigned_32 := 16#d00d_feed#;
   FDT_Tag_Size : constant Unsigned_32 := Unsigned_32'Size;

   FDT_Begin_Node : constant Unsigned_32 := 16#1#;
   FDT_End_Node   : constant Unsigned_32 := 16#2#;
   FDT_Prop       : constant Unsigned_32 := 16#3#;
   FDT_No_Op      : constant Unsigned_32 := 16#4#;
   FDT_End        : constant Unsigned_32 := 16#9#;

   FDT_V1_Size  : constant Unsigned_32 := 7 * Unsigned_32'Size;
   FDT_V2_Size  : constant Unsigned_32 := FDT_V1_Size + Unsigned_32'Size;
   FDT_V3_Size  : constant Unsigned_32 := FDT_V2_Size + Unsigned_32'Size;
   FDT_V16_Size : constant Unsigned_32 := FDT_V3_Size;
   FDT_V17_Size : constant Unsigned_32 := FDT_V16_Size + Unsigned_32'Size;

   function Get_Header_Size (FDT_Addr : System.Address) return Unsigned_32;

   type Assumption_Mask is new Unsigned_32;
   Assume_Mask        : constant Assumption_Mask := 16#0#;
   Assume_Perfect     : constant Assumption_Mask := 16#ff#;
   Assume_Valid_DTB   : constant Assumption_Mask := Shift_Left (1, 0);
   Assume_Valid_Input : constant Assumption_Mask := Shift_Left (1, 1);
   Assume_Latest      : constant Assumption_Mask := Shift_Left (1, 2);
   Assume_No_Rollback : constant Assumption_Mask := Shift_Left (1, 3);
   Assume_Order       : constant Assumption_Mask := Shift_Left (1, 4);
   Assume_Flawless    : constant Assumption_Mask := Shift_Left (1, 5);

   function Can_Assume (Mask : Assumption_Mask) return Boolean;

   function FDT_Version (FDT_Addr : System.Address) return Unsigned_32;
end FDT;
