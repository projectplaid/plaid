with Ada.Text_IO; use Ada.Text_IO;

with Arch;
with Board;
with Syscalls;
with UART;

with FDT;

with Last_Chance_Handler;

procedure Plaid is
begin
   Arch.Init;
   Board.Init;

   Put_Line ("Hello world");

   loop
      null;
   end loop;
end Plaid;
