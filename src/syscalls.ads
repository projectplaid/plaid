with Interfaces;

package Syscalls is
   type Capability_Index is new Interfaces.Unsigned_64;

   type Capability is record
      Index : Capability_Index;
   end record;

   type Priority is new Interfaces.Unsigned_8;
   Priority_Min : constant Priority := 0;
   Priority_Max : constant Priority := 255;

   type Message_Type is new Interfaces.Unsigned_32;

   type Message is record
      Source : Capability_Index;
      Typ    : Message_Type;
   end record;
end Syscalls;
