#!/bin/bash
qemu-system-riscv64 \
	-nographic \
	-machine virt \
	-bios u-boot-m-mode.bin \
	$*

